<?php

namespace App\Application\CommandBus\Bus;

use App\Application\CommandBus\Command\Command;

interface CommandBus {
    public function dispatch(Command $command): void;
}
