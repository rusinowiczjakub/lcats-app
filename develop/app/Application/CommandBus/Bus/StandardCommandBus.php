<?php

namespace App\Application\CommandBus\Bus;

use App\Application\CommandBus\Command\Command;
use App\Application\CommandBus\Handler\CommandHandlerLocator;

final class StandardCommandBus implements CommandBus {

    /**
     * @var CommandHandlerLocator
     */
    private $commandHandlerLocator;

    public function __construct(CommandHandlerLocator $commandHandlerLocator) {
        $this->commandHandlerLocator = $commandHandlerLocator;
    }

    public function dispatch(Command $command): void {
        ($this
            ->commandHandlerLocator
            ->getHandler($command)
        )->handle($command);
    }

}
