<?php

namespace App\Application\CommandBus\Handler;

use App\Application\CommandBus\Command\Command;

interface CommandHandlerLocator {
    public function getHandler(Command $command): CommandHandler;
}
