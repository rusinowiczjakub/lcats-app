<?php

namespace App\Application\CommandBus\Handler;

use App\Application\CommandBus\Command\Command;
use App\Application\CommandBus\Map\CommandHandlerMap;

final class StandardCommandHandlerLocator implements CommandHandlerLocator {

    /**
     * @var CommandHandlerMap
     */
    protected $commandHandlerMap;

    public function __construct(CommandHandlerMap $commandHandlerMap) {
        $this->commandHandlerMap = $commandHandlerMap;
    }

    public function getHandler(Command $command): CommandHandler {
        return $this->commandHandlerMap->getCommand(get_class($command));
    }


}
