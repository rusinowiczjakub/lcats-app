<?php

namespace App\Application\CommandBus\Handler;

use App\Application\CommandBus\Command\Command;

interface CommandHandler {
    public function handle(Command $command): void;
}
