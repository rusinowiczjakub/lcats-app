<?php

namespace App\Application\CommandBus\Map;

use App\Application\CommandBus\Handler\CommandHandler;
use Illuminate\Support\Collection;

final class CollectionCommandHandlerMap implements CommandHandlerMap {

    /**
     * @var Collection
     */
    private $commandHandlerCollection;

    /**
     * CollectionCommandHandlerMap constructor.
     * @param array $commandHandlerCollection
     */
    public function __construct(array $commandHandlerCollection) {
        $this->commandHandlerCollection = collect($commandHandlerCollection);
    }

    /**
     * @param $command
     * @return bool
     */
    public function commandExists($command) {
        return $this->commandHandlerCollection->offsetExists($command);
    }

    /**
     * @param string $command
     * @return CommandHandler
     * @throws \Exception
     */
    public function getCommand(string $command): CommandHandler {
        if (!$this->commandExists($command)) {
            // @TODO: Custom Exception
            throw new \Exception('Command not exists');
        }

        return $this->commandHandlerCollection->get($command);
    }
}
