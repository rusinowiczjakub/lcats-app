<?php

namespace App\Application\CommandBus\Map;

use App\Application\CommandBus\Handler\CommandHandler;

interface CommandHandlerMap {
    public function getCommand(string $command): CommandHandler;
}
