<?php

namespace App\Application\Auth;

use App\Application\CommandBus\Bus\CommandBus;
use App\Application\User\Command\CreateUserCommand;
use App\Infrastructure\Model\User\SocialAccount;
use App\Infrastructure\Model\User\User;
use Laravel\Socialite\Two\User as UserProvider;
use Ramsey\Uuid\Uuid;

class SocialAuthService {

    /**
     * @param UserProvider $userProvider
     * @param string $provider
     * @return SocialAccount | null
     */
    public function findSocialAccount(UserProvider $userProvider, string $provider): SocialAccount {
        return SocialAccount::where('provider_name', $provider)
            ->where('provider_id', $userProvider->getId())
            ->first();
    }

    public function handle(UserProvider $userProvider, string $provider) {
        /** @var CommandBus $commandBus */
        $commandBus = app(CommandBus::class);

        if ($socialAccount = $this->findSocialAccount($userProvider, $provider)) {
            return $socialAccount->user();
        }

        $user = null;

        if ($email = $userProvider->getEmail()) {
            $user = User::where('email', $email)->first();
        }

        if (!$user) {

            try {

                /*
                 * @TODO: Handle this more elegant
                 */
                $uuid = Uuid::uuid1();

                $user = User::create([
                     'id'         => $uuid,
                     'first_name' => $userProvider->getName(),
                     'last_name'  => $userProvider->getName(),
                     'email'      => $userProvider->getEmail()
                 ]);

                $commandBus->dispatch(
                    new CreateUserCommand(
                        $uuid,
                        $userProvider->getName(),
                        $userProvider->getName(),
                        null,
                        $userProvider->getEmail())
                );
            } catch (\Exception $exception) {

                /*
                 * @TODO: HANDLE IT MORE ELEGANT
                 */
                throw new \Exception('USER NOT CREATED');
            }

        }

            $user->socialAccounts()->create([
                'provider_id' => $userProvider->getId(),
                'provider_name' => $provider
            ]);

        return $user;
    }
}
