<?php

namespace App\Application\Auth;

use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Contracts\Auth\Authenticatable;

class SocialUserResolver implements SocialUserResolverInterface {
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable {
        $userProvider = null;

        try {
            $userProvider = \Socialite::driver($provider)->userFromToken($accessToken);
        } catch (\Exception $exception) {

        }

        return $userProvider ? (new SocialAuthService())->handle($userProvider, $provider) : null;
    }

}
