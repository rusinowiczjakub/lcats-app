<?php

namespace App\Application\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\Infrastructure\Model\User' => 'App\Policies\ModelPolicy'
    ];

    public function boot() {
        $this->registerPolicies();

        Passport::routes();
    }
}
