<?php

namespace App\Application\Providers;

use App\Application\Helpers\EventSerializer\EventSerializer;
use Illuminate\Support\ServiceProvider;

class ESServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->bind(EventSerializer::class, config('event-sourcing.event_serializer'));
    }
}
