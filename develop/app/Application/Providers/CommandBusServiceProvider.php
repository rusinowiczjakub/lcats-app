<?php

namespace App\Application\Providers;

use App\Application\CommandBus\Bus\StandardCommandBus;
use App\Application\CommandBus\Handler\StandardCommandHandlerLocator;
use App\Application\CommandBus\Map\CollectionCommandHandlerMap;
use Illuminate\Support\ServiceProvider;

class CommandBusServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->singleton('App\Application\CommandBus\Bus\CommandBus', function ($app) {
            return new StandardCommandBus(
                new StandardCommandHandlerLocator(
                    new CollectionCommandHandlerMap(
                        $app['config']['bus']['map']
                    )
                )
            );
        });
    }
}
