<?php

namespace App\Application\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider {
    public function register() {
        $this->app->bind(
            'App\Domain\User\Repository\UserRepositoryInterface',
            'App\Infrastructure\Eloquent\User\EloquentUserCheckRepository'
        );
    }
}
