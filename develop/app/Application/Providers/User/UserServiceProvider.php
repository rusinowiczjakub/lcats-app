<?php

namespace App\Application\Providers\User;

use App\Domain\User\UserService;
use App\Infrastructure\Eloquent\User\EloquentUserCheckRepository;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->singleton(UserService::class, function ($app) {
            return new UserService(new EloquentUserCheckRepository());
        });
    }

    public function provides() {
        return [UserService::class];
    }
}
