<?php

namespace App\Application\Helpers\EventSerializer;

use App\Infrastructure\Model\EventStorage\ShouldBeStored;

interface EventSerializer {
    public function serialize(ShouldBeStored $event): string;

    public function deserialize(string $eventClass, string $payload): ShouldBeStored;
}
