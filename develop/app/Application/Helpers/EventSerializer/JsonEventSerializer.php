<?php

namespace App\Application\Helpers\EventSerializer;

use App\Infrastructure\Model\EventStorage\ShouldBeStored;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonEventSerializer implements EventSerializer {

    /** @var Serializer */
    private $serializer;

    public function __construct() {
        $encoders = [new JsonEncoder()];
        $normalizers = [new PropertyNormalizer(), new DateTimeNormalizer()];

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function serialize(ShouldBeStored $event): string {
        if (method_exists($event, '__sleep')) {
            $event->__sleep();
        }

        return $this->serializer->serialize($event, 'json');
    }

    public function deserialize(string $eventClass, string $payload): ShouldBeStored {
        $event = $this->serializer->deserialize($payload, $eventClass, 'json');

        return unserialize(serialize($event));
    }


}
