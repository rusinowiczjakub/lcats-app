<?php

namespace App\Application\Console\Commands;

use App\Domain\ItemCheck\ItemCheck;
use App\Domain\ItemCheck\ValueObject\Voter;
use App\Domain\ItemCheck\ValueObject\VoteType;
use App\Domain\ItemCheck\Vote;
use App\Domain\User\User;
use App\Infrastructure\Model\EventStorage\EventStream;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jabadu:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        $uuid1 = '3337cf1c-c4e0-11e9-bf0b-0242ac120003';

        $uuid2 = '7fda6f78-c4e0-11e9-8207-0242ac120003';

        /** @var User $voteTarget */

        /** @var User $voteTarget */
        $voteTarget = User::retrieve($uuid1);

        /** @var User $voter */
        $voter = User::retrieve($uuid2);

        $voteTarget->verify();

        $voteTarget->vote(new \App\Domain\User\Vote($voter->getEmail(), \App\Domain\User\ValueObject\VoteType::LEGIT(), 'leeeegit'));

        $voteTarget->persist();

    }
}
