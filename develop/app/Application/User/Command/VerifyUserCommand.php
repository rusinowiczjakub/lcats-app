<?php

namespace App\Application\User\Command;

use App\Application\CommandBus\Command\Command;
use App\Domain\User\Command\VerifyUser;

final class VerifyUserCommand extends VerifyUser implements Command {}
