<?php

namespace App\Application\User\Command;

use App\Application\CommandBus\Command\Command;
use App\Domain\User\Command\VoteUser;

final class VoteUserCommand extends VoteUser implements Command {}
