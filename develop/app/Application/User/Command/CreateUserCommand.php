<?php

namespace App\Application\User\Command;

use App\Application\CommandBus\Command\Command;
use App\Domain\User\Command\CreateUser;

final class CreateUserCommand extends CreateUser implements Command {}
