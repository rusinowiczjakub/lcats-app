<?php

namespace App\Application\User\CommandHandler;

use App\Application\CommandBus\Command\Command;
use App\Application\CommandBus\Handler\CommandHandler;
use App\Domain\User\UserService;

final class CreateUserCommandHandler implements CommandHandler {
    public function handle(Command $command): void {
        /** @var UserService $service */
        $service = app(UserService::class);

        $service->createUser($command);
    }
}
