<?php

namespace App\Application\User\CommandHandler;

use App\Application\CommandBus\Command\Command;
use App\Application\CommandBus\Handler\CommandHandler;
use App\Domain\User\UserService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

final class VerifyUserCommandHandler implements CommandHandler {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function handle(Command $command): void {
        /** @var UserService $service */
        $service = app(UserService::class);

        $service->verify($command);
    }

}
