<?php

namespace App\Application\User\CommandHandler;

use App\Application\CommandBus\Command\Command;
use App\Application\CommandBus\Handler\CommandHandler;
use App\Domain\User\UserService;
use App\Infrastructure\Eloquent\User\EloquentUserCheckRepository;

final class VoteUserCommandHandler implements CommandHandler {
    /**
     * @param Command $command
     * @throws \App\Domain\User\Exception\UserAlreadyVotedException
     * @throws \App\Domain\User\Exception\UserNotVerifiedException
     */
    public function handle(Command $command): void {
        /** @var UserService $userService */
        $userService = app(UserService::class);

        $userService->voteUser($command);
    }
}
