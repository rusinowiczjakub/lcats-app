<?php

namespace App\Domain\Shared;

use App\Application\Helpers\EventSerializer\EventSerializer;
use App\Infrastructure\Model\EventStorage\EventStream;
use App\Infrastructure\Model\EventStorage\ShouldBeStored;

abstract class AggregateRoot implements AggregateInterface {

    protected $uuid;

    protected $events = [];

    public static function retrieve(string $uuid): self {
        $aggregate = (new static());

        $aggregate->uuid = $uuid;

        return $aggregate->fromEvents();
    }

    /**
     * @return EventStream
     */
    protected function getEventStreamClass(): string {
        return $this->eventStreamModel ?? config('event-sourcing.event_stream_model');
    }

    public function persist() {
        call_user_func(
            [$this->getEventStreamClass(), 'storeMultiple'],
            $this->events,
            $this->uuid
        );
    }

    protected function fromEvents() {
        /** @var EventSerializer $eventSerializer */
        $eventSerializer = app(EventSerializer::class);

        $this->getEventStreamClass()::uuid($this->uuid)
             ->each(function (EventStream $stream) use ($eventSerializer) {
                 $this->handle($eventSerializer->deserialize($stream->eventName, $stream->payload));
             });

        return $this;
    }

    abstract protected function handle(ShouldBeStored $event);
}
