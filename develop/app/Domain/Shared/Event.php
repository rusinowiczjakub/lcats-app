<?php

namespace App\Domain\Shared;

abstract class Event {
    abstract public function getAggregateType() :string;
}
