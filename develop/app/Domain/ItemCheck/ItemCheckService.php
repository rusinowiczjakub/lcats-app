<?php declare(strict_types = 1);

namespace App\Domain\ItemCheck;

use App\Domain\ItemCheck\Command\CreateItemCheck;
use App\Domain\ItemCheck\Command\VoteItemCheck;
use App\Domain\ItemCheck\Repository\ItemCheckRepositoryInterface;

class ItemCheckService {

    /** @var ItemCheckRepositoryInterface */
    private $itemsChecks;

    public function __construct(ItemCheckRepositoryInterface $itemCheckRepository) {
        $this->itemsChecks = $itemCheckRepository;
    }

    public function createItemCheck(CreateItemCheck $command) {
        $itemCheck = ItemCheck::create($command->getId(),
                                       $command->getName(),
                                       $command->getDescription(),
                                       $command->getQuestioning(),
                                       $command->getPhotos());

        $this->itemsChecks->save($itemCheck);
    }

    /**
     * @param VoteItemCheck $command
     * @throws Exception\UserAlreadyVotedException
     */
    public function vote(VoteItemCheck $command) {
        /** @var ItemCheck $itemCheck */
        $itemCheck = ItemCheck::retrieve($command->getVoteFor());

        $itemCheck->vote(new Vote($command->getVoter(), $command->getVoteType(), $command->getComment()));

        $this->itemsChecks->save($itemCheck);
    }
}
