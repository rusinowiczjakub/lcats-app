<?php

namespace App\Domain\ItemCheck\Command;

class CreateItemCheck {
    protected $id;

    protected $name;

    protected $description;

    protected $questioning;

    protected $photos;

    public function __construct($id,
                                $name,
                                $description,
                                $questioning,
                                $photos) {
        $this->id          = $id;
        $this->name        = $name;
        $this->description = $description;
        $this->questioning = $questioning;
        $this->photos      = $photos;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getQuestioning() {
        return $this->questioning;
    }

    /**
     * @return mixed
     */
    public function getPhotos() {
        return $this->photos;
    }


}
