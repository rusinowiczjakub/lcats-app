<?php

namespace App\Domain\ItemCheck\Command;

use App\Domain\ItemCheck\ValueObject\VoterInterface;
use App\Domain\ItemCheck\ValueObject\VoteType;

class VoteItemCheck {

    /** @var string */
    protected $voteFor;

    /** @var VoterInterface */
    protected $voter;

    /** @var VoteType */
    protected $voteType;

    /** @var string */
    protected $comment;

    public function __construct(string $voteFor,
                                VoterInterface $voter,
                                VoteType $voteType,
                                string $comment) {
        $this->voteFor  = $voteFor;
        $this->voter    = $voter;
        $this->voteType = $voteType;
        $this->comment  = $comment;
    }

    /**
     * @return string
     */
    public function getVoteFor(): string {
        return $this->voteFor;
    }

    /**
     * @return VoterInterface
     */
    public function getVoter(): VoterInterface {
        return $this->voter;
    }

    /**
     * @return VoteType
     */
    public function getVoteType(): VoteType {
        return $this->voteType;
    }

    /**
     * @return string
     */
    public function getComment(): string {
        return $this->comment;
    }
}
