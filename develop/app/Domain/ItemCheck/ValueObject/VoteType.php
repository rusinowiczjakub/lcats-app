<?php declare(strict_types = 1);

namespace App\Domain\ItemCheck\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * Class VoteType
 * @package App\Domain\ItemCheck
 *
 * @method static VoteType LEGIT()
 * @method static VoteType FAKE()
 */
class VoteType extends Enum {
    private const LEGIT = 1 ;
    private const FAKE = 0 ;
}
