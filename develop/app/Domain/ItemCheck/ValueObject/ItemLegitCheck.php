<?php declare(strict_types=1);

namespace App\Domain\ItemCheck\ValueObject;

class ItemLegitCheck {
    private $legit;

    private $fake;

    public function __construct()
    {
        $this->legit = 0;
        $this->fake  = 0;
    }

    public function addLegit(): void
    {
        $this->legit++;
    }

    public function addFake(): void
    {
        $this->fake++;
    }
}
