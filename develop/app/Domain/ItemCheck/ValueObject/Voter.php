<?php declare(strict_types = 1);

namespace App\Domain\ItemCheck\ValueObject;

class Voter implements VoterInterface, \JsonSerializable {

    /** @var string */
    private $email;

    /**
     * Voter constructor.
     * @param string $email
     */
    public function __construct(string $email) {
        $this->email = $email;
    }

    public function identify() {
        return $this->email;
    }

    public function jsonSerialize() {
        return $this->email;
    }


}
