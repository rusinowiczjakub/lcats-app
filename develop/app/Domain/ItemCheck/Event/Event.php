<?php declare(strict_types = 1);

namespace App\Domain\ItemCheck\Event;

use App\Domain\ItemCheck\ItemCheck;
use App\Domain\Shared\Event as SharedDomainEvent;
use App\Infrastructure\Model\EventStorage\ShouldBeStored;

abstract class Event extends SharedDomainEvent implements ShouldBeStored {
    public function getAggregateType(): string {
        return ItemCheck::class;
    }
}
