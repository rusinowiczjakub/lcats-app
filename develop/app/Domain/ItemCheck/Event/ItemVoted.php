<?php declare(strict_types = 1);

namespace App\Domain\ItemCheck\Event;

use App\Domain\ItemCheck\ValueObject\VoterInterface;
use App\Domain\ItemCheck\Vote;

class ItemVoted extends Event {

    /** @var Vote */
    protected $vote;

    /**
     * ItemVoted constructor.
     * @param VoterInterface $voter
     * @param Vote $vote
     */
    public function __construct(Vote $vote) {
        $this->vote  = $vote;
    }


    /**
     * @return Vote
     */
    public function getVote(): Vote {
        return $this->vote;
    }
}
