<?php

namespace App\Domain\ItemCheck\Repository;

use App\Domain\ItemCheck\ItemCheck;

interface ItemCheckRepositoryInterface {
    public function save(ItemCheck $itemCheck): void;

    public function get(string $id): ItemCheck;
}
