<?php declare(strict_types=1);

namespace App\Domain\ItemCheck;

use App\Domain\ItemCheck\Event\Event;
use App\Domain\ItemCheck\Event\ItemCheckCreated;
use App\Domain\ItemCheck\Event\ItemVoted;
use App\Domain\ItemCheck\Exception\UserAlreadyVotedException;
use App\Domain\ItemCheck\ValueObject\ItemLegitCheck;
use App\Domain\ItemCheck\ValueObject\VoteType;
use App\Domain\Shared\AggregateRoot;
use App\Infrastructure\Model\EventStorage\ShouldBeStored;

class ItemCheck extends AggregateRoot {

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var string */
    private $questioning;

    private $photos = [];

    /** @var Vote[] */
    private $votes = [];

    /**
     * ItemCheck constructor.
     */
    protected function __construct() {

    }

    /**
     * @param string $id
     * @param string $name
     * @param string $description
     * @param string $questioning
     * @param array $photos
     * @return ItemCheck
     */
    public static function create(string $id,
                                  string $name,
                                  string $description,
                                  string $questioning,
                                  array $photos): self {
        $itemCheck = new self();

        $itemCheck->record(new ItemCheckCreated($id, $name, $description, $questioning, $photos));

        return $itemCheck;
    }

    /**
     * @return string
     */
    public function identity() {
        return $this->uuid;
    }

    /**
     * @param Event $event
     */
    private function record(Event $event): void {
        $this->events[] = $event;
        $this->handle($event);
    }

    /**
     * @param ShouldBeStored $event
     */
    public function handle(ShouldBeStored $event): void {
//        $this->events[] = $event;
        switch (get_class($event)) {
            case ItemCheckCreated::class:
                /** @var ItemCheckCreated $event */
                $this->uuid        = $event->getId();
                $this->name        = $event->getName();
                $this->description = $event->getDescription();
                $this->questioning = $event->getQuestioning();
                $this->photos      = $event->getPhotos();

                break;

            case ItemVoted::class:
                /** @var ItemVoted $event */
                $this->votes[] = $event->getVote();

                break;
        }
    }

    /**
     * @param Vote $vote
     * @return void
     * @throws UserAlreadyVotedException
     */
    public function vote(Vote $vote): void {
        if ($this->hasUserVoted($vote)) {
            throw new UserAlreadyVotedException();
        }
        $this->record(new ItemVoted($vote));
    }

    /**
     * @return ItemLegitCheck
     */
    public function getVoteStatistics(): ItemLegitCheck {
        $itemLegitCheck = new ItemLegitCheck();

        /** @var $vote */
        foreach ($this->votes as $vote) {
            switch ($vote->getType()) {
                case VoteType::LEGIT():
                    $itemLegitCheck->addLegit();
                    break;

                case VoteType::FAKE():
                    $itemLegitCheck->addFake();
                    break;
            }
        }

        return $itemLegitCheck;
    }

    /**
     * @param Vote $newVote
     * @return bool
     */
    private function hasUserVoted(Vote $newVote): bool {
        foreach ($this->votes as $vote) {
            if ($vote->getFrom()->identify() === $newVote->getFrom()->identify()) {
                return true;
            }
        }

        return false;
    }

    public function getEvents() {
        return $this->events;
    }

}
