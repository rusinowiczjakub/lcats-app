<?php declare(strict_types=1);

namespace App\Domain\ItemCheck;

use App\Domain\ItemCheck\ValueObject\Voter;
use App\Domain\ItemCheck\ValueObject\VoterInterface;
use App\Domain\ItemCheck\ValueObject\VoteType;

class Vote implements \JsonSerializable {
    /** @var Voter */
    private $from;

    /** @var VoteType */
    private $voteType;

    /** @var string */
    private $comment;

    public function __construct(VoterInterface $from,
                                VoteType $voteType,
                                $comment
    ) {
        $this->from     = $from;
        $this->voteType = $voteType;
        $this->comment  = $comment;
    }

    public function getType() {
        return $this->voteType;
    }

    /**
     * @return Voter
     */
    public function getFrom(): Voter {
        return $this->from;
    }

    /**
     * @return VoteType
     */
    public function getVoteType(): VoteType {
        return $this->voteType;
    }

    /**
     * @return string
     */
    public function getComment(): string {
        return $this->comment;
    }

    public function jsonSerialize() {
//        return [
//            ''
//        ];
    }


}
