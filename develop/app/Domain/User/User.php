<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Shared\AggregateRoot;
use App\Domain\User\Event\Event;
use App\Domain\User\Event\UserCreated;
use App\Domain\User\Event\UserVerified;
use App\Domain\User\Event\Voted;
use App\Domain\User\Exception\UserAlreadyVotedException;
use App\Domain\User\Exception\UserNotVerifiedException;
use App\Domain\User\ValueObject\LegitCheck;
use App\Domain\User\ValueObject\UserStatus;
use App\Domain\User\ValueObject\VoteType;
use App\Infrastructure\Model\EventStorage\ShouldBeStored;

class User extends AggregateRoot {

    /** @var string */
    protected $name;

    /** @var string */
    protected $surname;

    /** @var \DateTimeImmutable */
    protected $birthDate;

    /** @var string */
    protected $email;

    /** @var UserStatus */
    protected $status;

    /** @var Vote[] */
    protected $votes = [];


    /**
     * User constructor.
     */
    protected function __construct() {
    }


    /**
     * @param string $uuid
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param \DateTimeImmutable $birthDate
     * @return User
     */
    public static function create(string $uuid,
                                  string $name,
                                  string $surname,
                                  string $email,
                                  \DateTimeImmutable $birthDate): self {
        $user = new self();

        $user->record(new UserCreated($uuid, $name, $surname, $birthDate, $email));

        return $user;
    }

    /**
     * @param Event $event
     */
    private function record(Event $event): void {
        $this->events[] = $event;
        $this->handle($event);
    }

    /**
     * @param ShouldBeStored $event
     */
    public function handle(ShouldBeStored $event): void {
        switch (get_class($event)) {
            case UserCreated::class:
                /** @var $event UserCreated */
                $this->uuid      = $event->getId();
                $this->email     = $event->getEmail();
                $this->name      = $event->getName();
                $this->surname   = $event->getSurname();
                $this->birthDate = $event->getBirthDate();
                $this->status    = UserStatus::NEW();
                $this->votes     = [];

                break;

            case UserVerified::class:
                $this->status = UserStatus::VERIFIED();

                break;

            case Voted::class:
                /** @var $event Voted */
                $this->votes[] = $event->getVote();

                break;
        }
    }

    /**
     * @param Vote $vote
     * @throws UserNotVerifiedException
     * @throws UserAlreadyVotedException
     */
    public function vote(Vote $vote): void {
        if (!$this->isVerified()) {
            throw new UserNotVerifiedException($this->email, $this->name, $this->surname);
        }

        if ($this->hasUserVoted($vote)) {
            throw new UserAlreadyVotedException($vote->getFrom(), $this->email);
        }

        $this->record(new Voted($vote->getFrom(), $this->email, $vote));
    }

    /**
     * @return void
     */
    public function verify(): void {
        $this->record(new UserVerified($this->email));
    }

    /**
     * @return bool
     */
    public function isVerified(): bool {
        return $this->status != UserStatus::NEW();
    }

    /**
     * @return Vote[]
     */
    public function getVotes(): array {
        return $this->votes;
    }

    /**
     * @return LegitCheck
     */
    public function getVotesStatistics(): LegitCheck {
        $legitCheck = new LegitCheck();

        /** @var Vote $vote */
        foreach ($this->votes as $vote) {
            switch ($vote->getType()) {
                case VoteType::LEGIT():
                    $legitCheck->addLegit();
                    break;

                case VoteType::FAKE():
                    $legitCheck->addFake();
                    break;
            }
        }

        return $legitCheck;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param Vote $vote
     * @return bool
     */
    public function hasUserVoted(Vote $vote) {
        foreach ($this->votes as $userVote) {
            if ($userVote instanceof Vote && $userVote->getFrom() == $vote->getFrom()) {
                return true;
            }
        }
        return false;
    }
}
