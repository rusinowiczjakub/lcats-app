<?php declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\User\Command\CreateUser;
use App\Domain\User\Command\VerifyUser;
use App\Domain\User\Command\VoteUser;
use App\Domain\User\Repository\UserRepositoryInterface;

class UserService
{

    /** @var UserRepositoryInterface */
    private $users;

    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
    }

    /**
     * @param CreateUser $command
     * @throws \Exception
     */
    public function createUser(CreateUser $command)
    {
        $user = User::create($command->getId(),
                             $command->getName(),
                             $command->getSurname(),
                             $command->getEmail(),
                             $command->getBirthDate()
            );

        $this->users->save($user);
    }

    /**
     * @param VoteUser $command
     * @throws Exception\UserNotVerifiedException
     * @throws Exception\UserAlreadyVotedException
     */
    public function voteUser(VoteUser $command)
    {
        /** @var User $receiver */
        $receiver = $this->users->get($command->getFor());

        $receiver->vote(new Vote($command->getFrom(),
                                 $command->getVoteType(),
                                 $command->getComment())
        );

        $this->users->save($receiver);
    }

    /**
     * @param VerifyUser $command
     */
    public function verify(VerifyUser $command) {
        $user = $this->users->get($command->getUuid());

        $user->verify();

        $this->users->save($user);
    }
}
