<?php declare(strict_types=1);

namespace App\Domain\User\Exception;

use Throwable;

class UserNotVerifiedException extends \Exception {
    public function __construct($email, $name, $surname, $code = 0, Throwable $previous = null) {
        parent::__construct(
            sprintf(
                'User %s %s [%s] account is not verified. Not verified users can not be voted',
                $name,
                $surname,
                $email),
            $code,
            $previous
        );
    }

}
