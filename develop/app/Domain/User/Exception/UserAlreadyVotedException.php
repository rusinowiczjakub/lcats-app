<?php declare(strict_types=1);

namespace App\Domain\User\Exception;

use Throwable;

class UserAlreadyVotedException extends \Exception {
    public function __construct($voterEmail, $receiverEmail, $code = 0, Throwable $previous = null) {
        parent::__construct(
            sprintf(
                'User with email %s already voted for %s',
                $voterEmail,
                $receiverEmail),
            $code,
            $previous
        );
    }

}
