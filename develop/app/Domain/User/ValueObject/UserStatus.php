<?php declare(strict_types=1);

namespace App\Domain\User\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * Class UserStatus
 * @package App\Domain\User
 *
 * @method static UserStatus BANNED()
 * @method static UserStatus NEW()
 * @method static UserStatus VERIFIED()
 */
class UserStatus extends Enum {
    private const BANNED = -1;
    private const NEW = 0;
    private const VERIFIED = 1;
}
