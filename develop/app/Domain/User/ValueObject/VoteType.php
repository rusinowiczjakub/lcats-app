<?php declare(strict_types = 1);

namespace App\Domain\User\ValueObject;

use MyCLabs\Enum\Enum;

/**
 * Class VoteType
 * @package App\Domain\User
 *
 * @method static VoteType LEGIT()
 * @method static VoteType FAKE()
 */
class VoteType extends Enum {
    private const LEGIT = 1;
    private const FAKE = 0;
}
