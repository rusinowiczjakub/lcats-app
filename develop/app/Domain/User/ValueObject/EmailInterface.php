<?php declare(strict_types = 1);

namespace App\Domain\User\ValueObject;

interface EmailInterface {

    public function getEmail(): string;
}
