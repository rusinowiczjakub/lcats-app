<?php declare(strict_types=1);

namespace App\Domain\User\ValueObject;

class LegitCheck {
    private $legit;

    private $fake;

    public function __construct()
    {
        $this->legit = 0;
        $this->fake  = 0;
    }

    public function addLegit()
    {
        $this->legit++;
    }

    public function addFake()
    {
        $this->fake++;
    }

    public function getLs()
    {
        return $this->legit;
    }

    public function getFs()
    {
        return $this->fake;
    }
}
