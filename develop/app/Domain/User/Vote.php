<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\User\ValueObject\VoteType;

class Vote {

    /** @var string */
    private $from;

    /** @var VoteType */
    private $voteType;

    /** @var string */
    private $comment;

    public function __construct($from,
                                VoteType $voteType,
                                $comment
    ) {
        $this->from     = $from;
        $this->voteType = $voteType;
        $this->comment  = $comment;
    }

    public function getType() {
        return $this->voteType;
    }

    public function getFrom() {
        return $this->from;
    }
}
