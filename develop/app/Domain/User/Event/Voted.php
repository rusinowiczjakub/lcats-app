<?php declare(strict_types = 1);

namespace App\Domain\User\Event;

use App\Domain\User\Vote;

class Voted extends Event {

    /** @var string */
    private $voter;

    /** @var string */
    private $email;

    /** @var Vote */
    private $vote;

    public function __construct(string $voter, string $email, Vote $vote) {
        $this->voter = $voter;
        $this->email = $email;
        $this->vote = $vote;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @return Vote
     */
    public function getVote(): Vote {
        return $this->vote;
    }

    /**
     * @return string
     */
    public function getVoter() {
        return $this->voter;
    }

}
