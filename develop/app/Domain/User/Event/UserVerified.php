<?php declare(strict_types=1);

namespace App\Domain\User\Event;

class UserVerified extends Event {

    /** @var string */
    private $email;

    public function __construct(string $email) {
        $this->email = $email;
    }
}
