<?php declare(strict_types=1);

namespace App\Domain\User\Event;

class UserCreated extends Event {

    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $surname;

    /** @var \DateTimeImmutable */
    protected $birthDate;

    /** @var string */
    protected $email;

    /**
     * UserCreated constructor.
     * @param string $id
     * @param string $name
     * @param string $surname
     * @param \DateTimeImmutable $birthDate
     * @param string $email
     */
    public function __construct(string $id,
                                string $name,
                                string $surname,
                                \DateTimeImmutable $birthDate,
                                string $email) {
        $this->id        = $id;
        $this->name      = $name;
        $this->surname   = $surname;
        $this->birthDate = $birthDate;
        $this->email     = $email;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string {
        return $this->surname;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getBirthDate(): \DateTimeImmutable {
        return $this->birthDate;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }
}
