<?php declare(strict_types=1);

namespace App\Domain\User\Event;

use App\Domain\Shared\Event as SharedDomainEvent;
use App\Domain\User\User;
use App\Infrastructure\Model\EventStorage\ShouldBeStored;

abstract class Event extends SharedDomainEvent implements ShouldBeStored {
    public function getAggregateType(): string {
        return User::class;
    }
}
