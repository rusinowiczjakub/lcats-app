<?php

namespace App\Domain\User\Command;

class VerifyUser {

    /** @var string */
    protected $uuid;

    public function __construct(string $uuid) {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getUuid(): string {
        return $this->uuid;
    }
}
