<?php

namespace App\Domain\User\Command;

use App\Domain\User\ValueObject\VoteType;

class VoteUser {
    /** @var string */
    protected $from;

    /** @var string */
    protected $for;

    /** @var VoteType */
    protected $voteType;

    /** @var string */
    protected $comment;

    public function __construct($from,
                                $for,
                                VoteType $voteType,
                                $comment
    ) {
        $this->from     = $from;
        $this->for      = $for;
        $this->voteType = $voteType;
        $this->comment  = $comment;
    }

    /**
     * @return string
     */
    public function getFrom(): string {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getFor(): string {
        return $this->for;
    }

    /**
     * @return VoteType
     */
    public function getVoteType(): VoteType {
        return $this->voteType;
    }

    /**
     * @return string
     */
    public function getComment(): string {
        return $this->comment;
    }


}
