<?php

namespace App\Domain\User\Command;

class CreateUser {
    protected $id;

    protected $name;

    protected $surname;

    protected $birthDate;

    protected $email;

    public function __construct($id,
                                $name,
                                $surname,
                                $birthDate,
                                $email
    ) {
        $this->id        = $id;
        $this->name      = $name;
        $this->surname   = $surname;
        $this->birthDate = $birthDate;
        $this->email     = $email;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSurname() {
        return $this->surname;
    }

    /**
     * @return mixed
     */
    public function getBirthDate() {
        return $this->birthDate;
    }

    /**
     * @return mixed
     */
    public function getEmail() {
        return $this->email;
    }


}
