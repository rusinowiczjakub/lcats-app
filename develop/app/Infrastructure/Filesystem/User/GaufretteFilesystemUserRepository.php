<?php declare(strict_types = 1);

namespace App\Infrastructure\Filesystem\User;

use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\User;
use Gaufrette\Filesystem;

class GaufretteFilesystemUserRepository implements UserRepositoryInterface {

    /** @var Filesystem */
    private $filesystem;

    public function __construct(Filesystem $filesystem) {
        $this->filesystem = $filesystem;
    }

    public function save(User $user): void {
        $this->filesystem->write($user->getEmail(), serialize($user), true);
    }

    public function get(string $email): User {
        return unserialize($this->filesystem->read($email));
    }

}
