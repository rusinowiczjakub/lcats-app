<?php

namespace App\Infrastructure\Filesystem\ItemCheck;

use App\Domain\ItemCheck\ItemCheck;
use App\Domain\ItemCheck\Repository\ItemCheckRepositoryInterface;
use Gaufrette\Filesystem;

class GaufretteFilesystemItemCheckRepository implements ItemCheckRepositoryInterface {

    protected $filesystem;

    public function __construct(Filesystem $filesystem) {
        $this->filesystem = $filesystem;
    }

    public function save(ItemCheck $itemCheck): void {
        $this->filesystem->write($itemCheck->identity(), serialize($itemCheck), true);
    }

    public function get(string $id): ItemCheck {
        return unserialize($this->filesystem->read($id));
    }


}
