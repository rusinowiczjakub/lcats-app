<?php

namespace App\Infrastructure\Eloquent\User;

use App\Domain\ItemCheck\ItemCheck;
use App\Domain\ItemCheck\Repository\ItemCheckRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\User;

class EloquentUserCheckRepository implements UserRepositoryInterface {
    public function save(User $user): void {
        $user->persist();
    }

    public function get(string $uuid): User {
        /** @var User $user */
        $user = User::retrieve($uuid);

        return $user;
    }
}
