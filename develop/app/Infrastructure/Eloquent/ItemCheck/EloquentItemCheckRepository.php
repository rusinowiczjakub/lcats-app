<?php

namespace App\Infrastructure\Eloquent\ItemCheck;

use App\Domain\ItemCheck\ItemCheck;
use App\Domain\ItemCheck\Repository\ItemCheckRepositoryInterface;

class EloquentItemCheckRepository implements ItemCheckRepositoryInterface {
    public function save(ItemCheck $itemCheck): void {
        $itemCheck->persist();
    }

    public function get(string $id): ItemCheck {
        /** @var ItemCheck $itemCheck */
        $itemCheck = ItemCheck::retrieve($id);

        return $itemCheck;
    }

}
