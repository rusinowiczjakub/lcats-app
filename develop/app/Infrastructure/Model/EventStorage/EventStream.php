<?php

namespace App\Infrastructure\Model\EventStorage;

use App\Application\Helpers\EventSerializer\EventSerializer;
use App\Application\Helpers\EventSerializer\JsonEventSerializer;
use App\Domain\Shared\Event;
use App\Infrastructure\Model\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Ramsey\Uuid\Uuid;

/**
 * Class EventStream
 * @package App\Infrastructure\Model\EventStorage
 *
 * @method static uuid(string $uuid)
 *
 * @property $payload
 * @property $aggregateId
 * @property $eventName
 * @property $createdAt
 * @property $eventId
 * @property $aggregateType
 */
class EventStream extends BaseModel {
    protected $table = 'event_stream';

    protected $primaryKey = 'event_id';

    public $timestamps = false;

    public $guarded = [];

    public static function createForEvent(ShouldBeStored $event, string $uuid = null): self {
        /** @var JsonEventSerializer $eventSerializer */
        $eventSerializer = app(EventSerializer::class);
//        dump($event);

        $eventStream                = new static();
        $eventStream->eventId       = (Uuid::uuid1())->toString();
        $eventStream->aggregateId   = $uuid;
        $eventStream->aggregateType = $event instanceof Event ? $event->getAggregateType() : null;
        $eventStream->eventName     = get_class($event);
        $eventStream->payload       = $eventSerializer->serialize($event);
        $eventStream->createdAt     = Carbon::now();

        $eventStream->save();

        return $eventStream;
    }

    /**
     * @param $query
     * @param string $uuid
     */
    public static function scopeUuid($query, string $uuid): void {
        /** @var Builder $query */
        $query->where('aggregate_id', $uuid)
            ->orderBy('created_at', 'ASC');
    }

    public function getEventAttribute(): ShouldBeStored {
        return unserialize($this->payload);
    }

    public static function storeMultiple(array $events, string $uuid = null): void {
        collect($events)
            ->map(function (ShouldBeStored $domainEvent) use ($uuid) {
                $storedEvent = static::createForEvent($domainEvent, $uuid);

                return [$domainEvent, $storedEvent];
            });
//            ->eachSpread(function (ShouldBeStored $event, EventStream $storedEvent) {
        /* Handle creating projection here */
//            });
    }

    public static function store(ShouldBeStored $event, string $uuid = null) {
        static::storeMultiple([$event], $uuid);
    }
}
