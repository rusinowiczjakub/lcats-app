<?php

namespace App\Infrastructure\Model\EventStorage;

/**
 * Interface ShouldBeStored
 * @package App\Infrastructure\Model\EventStorage
 */
interface ShouldBeStored {}
