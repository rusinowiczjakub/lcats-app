<?php

namespace App\Infrastructure\Model\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialAccount
 * @package App\Infrastructure\Model\User
 * @mixin \Eloquent
 */
class SocialAccount extends Model {
    protected $fillable = [
        'provider_name',
        'provider_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
