<?php

namespace App\Ui\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ItemCheckController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
