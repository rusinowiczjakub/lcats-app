<?php

namespace App\Http\Controllers;

use App\Application\CommandBus\Bus\CommandBus;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    /**
     * @var CommandBus $commandBus
     */
    protected $commandBus;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $this->commandBus = app(CommandBus::class);
    }
}
