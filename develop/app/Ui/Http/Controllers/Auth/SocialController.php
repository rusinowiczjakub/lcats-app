<?php

namespace App\Ui\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class SocialController extends Controller {
    public function redirect($provider) {
        return \Socialite::driver($provider)->redirect();
    }

    public function callback($provider) {

    }
}
