<?php

namespace App\Ui\Http\Controllers\User;

use App\Application\User\Command\VoteUserCommand;
use App\Domain\User\Exception\UserAlreadyVotedException;
use App\Domain\User\Exception\UserNotVerifiedException;
use App\Domain\User\UserService;
use App\Domain\User\ValueObject\VoteType;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class VoteFake extends Controller {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __invoke(Request $request, $uuid) {

        try {
            /*
             * Temporary hardcoding *LOGGED USER* till creating auth
             */
            $voter = 'monikamilner1@gmail.com';

            $command = new VoteUserCommand($voter,
                                           $uuid,
                                           VoteType::FAKE(),
                                           $request->input('comment')
            );

            $this->commandBus->dispatch(
                $command
            );
        } catch (UserNotVerifiedException | UserAlreadyVotedException $exception) {
            return response()
                ->json([
                           'status'  => 'error',
                           'message' => $exception->getMessage()
                       ])->setStatusCode(200);
        }
    }
}
