<?php

namespace App\Ui\Http\Controllers\User;

use App\Application\User\Command\CreateUserCommand;
use App\Http\Controllers\Controller;
use DateTimeImmutable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class CreateUser extends Controller {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function __invoke(Request $request) {
        $uuid    = Uuid::uuid1();
        $command = new CreateUserCommand($uuid->toString(),
                                         $request->input('name'),
                                         $request->input('surname'),
                                         new DateTimeImmutable($request->input('birthDate')),
                                         $request->input('email')
        );

        $this->commandBus->dispatch($command);
    }
}
