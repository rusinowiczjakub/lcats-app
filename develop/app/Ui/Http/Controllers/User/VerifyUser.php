<?php

namespace App\Ui\Http\Controllers\User;

use App\Application\User\Command\CreateUserCommand;
use App\Application\User\Command\VerifyUserCommand;
use App\Http\Controllers\Controller;
use DateTimeImmutable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class VerifyUser extends Controller {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __invoke(Request $request, $uuid) {
        $command = new VerifyUserCommand($uuid);

        $this->commandBus->dispatch($command);
    }
}
