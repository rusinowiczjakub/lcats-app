<?php

use App\Domain\ItemCheck\Command\CreateItemCheck;
use App\Domain\ItemCheck\Command\VoteItemCheck;
use App\Domain\ItemCheck\ItemCheckService;
use App\Domain\ItemCheck\ValueObject\Voter;
use App\Infrastructure\Filesystem\ItemCheck\GaufretteFilesystemItemCheckRepository;
use App\Infrastructure\Model\EventStorage\EventStream;
use Gaufrette\Adapter\Local as LocalAdapter;
use Gaufrette\Filesystem;
use Ramsey\Uuid\Uuid;

require_once 'vendor/autoload.php';

//dump(storage_path());
////DATA STORAGE
//$adapter = new LocalAdapter('storage/app/users', true, 0750);
//$filesystem = new Filesystem($adapter);
//
//REPOSITORY
//$respository = new GaufretteFilesystemUserRepository($filesystem);
//
//$service = new UserService($respository);
////
//try {
//    $service->createUser(new CreateUser('Jakub', 'Rusinowicz', '01-03-1995', 'rusinowiczjakub@gmail.com'));
//} catch (Exception $e) {
//    dump($e->getMessage());
//}
////
////dump($respository->get('rusinowiczjakub@gmail.com'));
//
//try {
//    $service->voteUser(new VoteUser('test@test.pl', 'rusinowiczjakub@gmail.com', VoteType::LEGIT(), 'test'));
//} catch (UserNotVerifiedException $e) {
//    dump($e);
//}
//
//$service->verify(new VerifyUser('rusinowiczjakub@gmail.com'));
//
//try {
//    $service->voteUser(new VoteUser('test@test.pl', 'rusinowiczjakub@gmail.com', VoteType::LEGIT(), 'test'));
//    $service->voteUser(new VoteUser('test1@test.pl', 'rusinowiczjakub@gmail.com', VoteType::LEGIT(), 'test'));
//} catch (UserNotVerifiedException $e) {
//    dump($e);
//}
//
//
//dump($respository->get('rusinowiczjakub@gmail.com')->getVotes());



//$adapter = new LocalAdapter('storage/app/item-checks', true, 0750);
//$filesystem = new Filesystem($adapter);
//
//$repository = new GaufretteFilesystemItemCheckRepository($filesystem);
//
//$uuid = Uuid::uuid1();
//
//$itemCheckService = new ItemCheckService($repository);
//
//
//$itemCheck = ItemCheck::create(
//    $uuid,
//    'Supreme BOGO Hoodie',
//    'test',
//    'rusinowiczjakub@gmail.com',
//    []
//);
//
//$itemCheckService->createItemCheck(
//    new CreateItemCheck($uuid->toString(),
//                        'Supreme BOGO Hoodie',
//                        'test',
//                        'rusinowiczjakub@gmail.com',
//                        [])
//);
//
//$itemCheckService->vote(new VoteItemCheck($uuid, new Voter('rusinowiczjakub@gmail.com'), \App\Domain\ItemCheck\ValueObject\VoteType::LEGIT(), 'Legia'));
//
//dump(($repository->get($uuid))->getVoteStatistics());
//
//$itemCheck->vote(new Vote(new Voter('test1@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
//$itemCheck->vote(new Vote(new Voter('test2@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
//$repository->save($itemCheck);
//
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::LEGIT(), 'test'));
////$itemCheck->vote(new Vote(new Voter('test@test.pl'), \App\Domain\ItemCheck\VoteType::FAKE(), 'test'));
//$itemCheck->vote(new Vote(new Voter('test3@test.pl'), \App\Domain\ItemCheck\VoteType::FAKE(), 'test'));
//$itemCheck->vote(new Vote(new Voter('test4@test.pl'), \App\Domain\ItemCheck\VoteType::FAKE(), 'test'));
////dump($itemCheck->getVoteStatistics());
//
//
//dump($repository->get($uuid)->getVoteStatistics());
