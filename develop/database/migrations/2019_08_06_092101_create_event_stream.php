<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventStream extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_stream', function (Blueprint $table) {
            $table->uuid('event_id')->primary();
//            $table->integer('version')->unsigned();
            $table->string('event_name', 255);
            $table->text('payload');
            $table->uuid('aggregate_id');
            $table->string('aggregate_type', 255);
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_stream');
    }
}
