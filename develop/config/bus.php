<?php

use App\Application\User\Command\CreateUserCommand;
use App\Application\User\Command\VerifyUserCommand;
use App\Application\User\Command\VoteUserCommand;
use App\Application\User\CommandHandler\CreateUserCommandHandler;
use App\Application\User\CommandHandler\VerifyUserCommandHandler;
use App\Application\User\CommandHandler\VoteUserCommandHandler;

return [
    'map' => [
        /* USER DOMAIN COMMAND => HANDLER MAPPING */
        CreateUserCommand::class => new CreateUserCommandHandler(),
        VerifyUserCommand::class => new VerifyUserCommandHandler(),
        VoteUserCommand::class   => new VoteUserCommandHandler(),
    ]
];
