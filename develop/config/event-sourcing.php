<?php

use App\Application\Helpers\EventSerializer\JsonEventSerializer;
use App\Infrastructure\Model\EventStorage\EventStream;

return [
    'event_stream_model' => EventStream::class,

    'event_serializer' => JsonEventSerializer::class
];
