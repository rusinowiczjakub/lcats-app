<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('/auth/register', 'UserCheck\CreateUser');
//Route::post('/auth/register', 'User\CreateUser');
Route::post('/user/{uuid}/verify', 'User\VerifyUser');
Route::post('/user/{uuid}/vote/fake', 'User\VoteFake');
Route::post('/user/{uuid}/vote/legit', 'User\VoteLegit');
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



//Route::get('/vote/user/{uuid}/legit', '\App\Ui\Http\Controllers\VoteLegit');
