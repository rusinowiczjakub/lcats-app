# Streetwear LEGIT CHECKS APP Api

### Creating new domain

1. Create new directory in `app/Domain/{YOUR_DOMAIN_NAME}`
1. In case you want Events to be stored in **Event Store** extend your Domain Aggregate with abstract `App\Domain\Shared\AggregateRoot`
1. Events which you want to be stored in **Event Store** should implement interface `App\Infrastructure\Model\EventStorage\ShouldBeStored`;
1. For events stored in **Event Store** I suggest to extend them with abstract `App\Domain\Shared\Event` and implement method `getAggregateType()` eg. in this way:

    ```php
        public function getAggregateType(): string {
            return User::class;
        }
    ```
    It will facilitate recognition of which aggregate a given event belongs to, but it's not required.

